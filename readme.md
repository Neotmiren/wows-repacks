# World of Warcraft Server Repacks

  Files available in download section represent well thought-out and quick way
  1) to unpack/run/stop any World of Warcraft server of multiple versions
  compiled from various open-source projects, 2) to save/restore game state,
  and 3) to save GBytes of disk space by compressing similar data grouped.
  Not intended for unknowledgeable users.
  
  Included repacks implement server side for old WoW client versions (no longer
  supported by official servers). The repacks are configured for single
  player and can instantly serve as means to explore game world in GM mode.

  Minimum system requirements: CPU with SSE or SSE2 support (depends on repack),
  32-bit Windows XP with 1-2 GB RAM. Visual C++ runtime libraries 2008 and 2010
  must be installed. No need to install other packages - all required files are
  included and just have to be unpacked.

## Supported World of Warcraft client versions and available Server Cores

* Classic 1.12 - cMangos Multiclass/Single Player (see details below) [+playerbots], Mangos-Zero (old), Nostalrius/LightsHope
* The Burning Crusade 2.4.3 - HGcore, Oregon Core, CMaNGOS [+playerbots]
* Wrath of the Lich King 3.3.5a - Sunwell Core Multiclass/Single Player, Quantum Core, Trinity Core [+npcbots], MangosR2 [+playerbots], Arcemu
* Cataclysm 4.0.6a - Skyfire, Arkania
* Cataclysm 4.3.4 - Trinity Core [+npcbots]

  All repacks are blizzlike (no custom content), except the Arcemu repack,
  which is included as example of Arcemu, and of how bad are custom/"fun"
  repacks are in general... at least if created by people unable to
  understand the true spirit of high fantasy worlds. Such inability is also
  related to developers of official Cataclysm and later WoW expansions as
  whole, so "blizzlike" status does not help much there...

  3.3.5a MangosR2 and ScreenEmu 4.3.4 repacks may be interesting because
  they use YTDB, which has differing NPC layouts and quite a lot data
  missing in Trinity databases.

## Installation

  Go to download section and get following files:

* WowFileStructures.7z
* WowExternalExecutables.7z
* WoWServerRepacks.7z
* dbc.7z  (with some patches applied to data extracted from client)
* maps.7z
* vmaps.7z
* mmaps-1.12-cMangos.7z
* mmaps-2.4.3-HGcore.7z
* mmaps-3.3.5a-Sunwell.7z
* ... and other mmaps; all mmaps are recommended but optional

  WowExternalExecutables.7z archive contains some programs that must
  be placed to any of directories listed in PATH environment variable. This
  archive contains some necessary utilities (like command-line archivers),
  portable HeidiSQL, portable PHP. The latter is needed to run command-line
  PHP scripts that work with Server database, providing much better way
  of dynamic patching for repacks (comparing to .sql files), and allowing
  other things like to list creatures that drop specific item etc.

  WowFileStructures.7z archive contains injections to directory structures
  for all supported versions of WoW clients. Add files from this archive to
  corresponding places in directories of your existing World of Warcraft
  client versions. Some essential files, like WoW-1.12\repack\dbc.7z, have
  zero length and must be replaced by archives from the above list.

  .cmd, .php, .lua files and subdirectories with identical names have
  identical content. It is possible to link them on NTFS volumes using file
  hardlinks and directory junction links. Linking is not necessary, but
  having more than one client version and duplicating .\repack\ subdirectory
  in each client will waste disk space...

  Optional step. Open `wows-where.cmd` file and edit it, changing list of
  directory paths for unpacked server + mysql + maps etc. Default locations are
  "Q:\" "C:\TEMP\" "C:\". Drive Q: is supposed to be RAM drive with about 2 GB
  free space or more; can be very useful if alot of RAM is installed. It is
  also possible to use  subst Q: "X:\Custom\Path"  system command to set custom
  path without editing `wows-where.cmd` file.

  Optional step. Run `wows-where.cmd create` - it should detect game client
  version and print the path to directory where WoW server will be
  unpacked.

## Use

* Run `wows-unpack.cmd` without parameters to unpack default server repack for
  given WoW version (all default repack names are assigned in this .cmd file
  along with apporopriate dbc, maps, vmaps and mmaps for each repack).
  `wows-unpack.cmd _repackname_` unpacks specific repack with _repackname_
  matching to one of folders inside "WoW Sever Repacks.7z" archive.
  `wows-patch.cmd` and `patch-<WoWversion>.php` will be called automatically.

* Run `wows-restore.cmd [optional .rar savegame archive name]` to restore World
  of Warcraft state from existing savegame archive. Most repacks do not require
  this step (they already have account "a" with password "a" in database, and a
  few might require it. In the worst case you will need to create account by
  yourself using following commands in worldserver console window:
```text
    .account create a a
    .account set gmlevel a 3 -1
    .account set addon a 1    -- might be needed for TBC
    .account set addon a 2    -- might be needed for WotLK
```

* Run `wows-run.cmd` to start mysql server window, authserver window,
  worldserver window and World of Warcraft game client. This script will start
  all programs from said list of four if they are not already started.
  `wows-transmogrify.cmd` will be called automatically before starting WoW.exe
  and will modify model numbers in WDB cache, but only after targeted items
  have been cached already (may take effect after restart).

* Enter "a" as login and "a" as password. If login failed Run HeidiSQL and
  check presence of account "a" in `auth/realmd` database, TABLE `accounts`.
  Create account from worldserver window console if it does not exist.

* Play World of Warcraft.

* After logging off from World of Warcraft, Run `wows-save.cmd` to save current
  world state as .rar savegame archive with automatically increasing number.
  This file can be used to restore world state at any time.


## Notes

* Classic 1.12 client Data/Interface/FrameXML/ .lua and .xml files are changed
  to fix some bugs and to support important single player mode improvements
  (made more than 16 buffs visible, reduced overflowing number of stance bar
  icons, changed their order, etc.). The patched version of wow.exe, with
  removed MD5 & SIG checks, must be used, or "FrameXML is modified or corrupt"
  error is inevitable.

* `wows-restore.cmd` is able to restore savegames not only from separate .rar
  files but also from repacked bundles like wows-1.12--bundle@.rar
  containing many savegames in their respective subdirectories.

* `wows-patch.cmd` re-extracts database and patches it.
  `wows-patch.cmd unpatch` re-extracts clean database.
  `wows-patch.cmd initial` patches clean database without re-extracting it.

* `wows-patch.cmd` can also be used to load .sql and .sql.7z files or
  immediate SQL statements from command line.

* `wows-unpack.cmd` can also be used to extract additional MySQL databases
  from archives (for purposes of development and comparision).

* `wows-transmogrify.cmd` can be edited to set desired transmogrifications for
  items by changing their model numbers in itemcache.wdb file. Such a
  transmogrification is limited to local computer, but for single player it
  is much more convenient than any in-game transmogrification.

* WowFileStructures.7z archive contains small patches for all five versions of
  wow.exe in form of text files (format is compatible with output from fc.exe
  command line utility) located in `WoW-112\stuff\_devel\WoW.exe.patches.rar`
  subarchive. With help of fcpatch.bat script from WowExternalExecutables.7z
  (miniperl.exe interpreter provided) it is possible to check, unpatch or
  patch wow.exe using these text files.

* Wow-112/Data/patch-i.MPQ contains spell.DBC with a few modifications, that
  fix things, mostly server-side. It is useful only for multiclass single
  player, and even multiclass single player may play without it, but there
  might be some annoyances, like cooldown collisions with certain spells
  (Priest's Power Word: Shield and Paladin's Lay on Hands, for example).

* Purely optional Wow-112/Data/patch-m.MPQ contains WotLK model for Lady
  Sylvanas Windrunner and nicer Hunter's Mark animation.

* Login:password to access all databases is root:ascent


# About cMangos Classic WoW 1.12 Multiclass Single Player repack

The repack is intended for multiclass single player game. After logging
off to WoW title screen, you can change class and/or race using a command like

D:\Games\WoW-112> wows-class.cmd PlayerName Paladin Dwarf

Regardless of what class the player currently uses, he/she has access to
spells, abilities and talents of ALL classes (maybe except some starting
rank 1 spells) at the same time. Player can wear any armor, use shield, any
combination of weapons, any ranged weapon or wand, use Rogue Stealth and
Pickpocket, summon Hunter Pet or Warlock Minions, heal self or heal summoned
pet. All these possibilities result in much more rich and interesting game.

It is necessary to switch classes in order to learn new abilities
from trainers and to distribute talent points gained from levelups.

Multiclass Repack uses modified cMangos core. Serious game-breaking issues,
which plagued the core since about 2012, have been completely fixed here. And
thereafter only trustworthy commits were ported from CMaNGOS repository,
effectively excluding newly introduced regressions, which become reported in
the issues section of their repository. Many previously unsupported talents,
scripts and other features are implemented now (with help of code from variuos
other WoW server emulation projects). Database is basically filtered version of
CMaNGOS ClassicDB (excluding, in most cases, their new creature stats from so
called "Bestiary", which lead to less hardcore, less unpredictable world) with
two separated layers of additional fixes and modifications
(worldpatch-1.12.sql.7z and patch-112.php). Some important creature data have
been ported from Nostalrius/Elysium/LightsHope DB, especially the data related
to raid instances.

>Note, that __all__ the fixes will only become available through using
`wows-unpack.cmd`, which ensures that server runs with patched DBC files
(some spells are fixed this way), with database modifications from
worldpatch-1.12.sql.7z (static .sql) and patch-112.php (dynamic code).

It is worth noting that cMangos stands as much better implementation of
Classic content than Trinity Core or any other 3.3.5a repack... even not
counting "hardcoreness" of the original content (lots of elite outdoor
creatures, original class quest chains etc), which makes it way more
interesting than dull nerfed 3.3.5a level 1-60 content.

Most important Single Player / Multiclass -oriented modifications:

* Removed class restrictions from all quests. Player does not need to change
  classes because of quests.
* Greatly slowed Level progression: required XP per level is multiplied
  x 4-10 times (depending on level with low levels slowed the most, which
  urges travelling to starting zones of allied factions and completing quests
  there). Single Player game should not be focused on end-game raids. It is
  a long adventure; player can grind creatures faster and is powerful
  enough to solo elites and dungeon instances of equal and even higher level.
  There are no problems with game balance while leveling, because there are
  always enough difficult creatures of higher levels.
* Increased x2 damage from Normal, Rare and Rare Elite overworld creatures.
  Normal ones are still weaker than Elite creatures, and their HP remains
  usual. Non-elite overworld spellcasters become much more dangerous.
* Increased XP for exploring new areas.
* Reduced Rested XP gain rate and upper limit.
* Greatly prolonged respawn times of all creatures (except civilians and
  critters). Fast respawn rates in overworld are unavoidable in multiplayer,
  but in single player game the slow pace, when player has enough time to
  admire the scenery, is much preferable. Mindless grinding in a "good spot" is
  no more an option. As for dungeon and raid instances, the much slower respawn
  rates are better because single player has lower DPS, comparing to whole
  party, so he/she needs more time. And at last, you should be allowed to take
  a break in single player game, why not?
* Prolonged respawn times of resource nodes, chests, footlockers etc. - in
  cases they respawn too fast for single player game, which should not turn
  into meaningless grind.
* Talent rate x8 (number of classes). The depth allowed in each talent tree
  is limited by player's level. Choosing talents from 24 trees, player must
  think out choices much more carefully then before. And it is really
  interesting because such things are far beyond well-known patterns.
* Reduced Pet XP per level factor. Now can easily train two pets simultaneously
  while leveling.
* Reduced (in a consistent way) damage and HP from Raid creatures and
  worldbosses, including known non-static bosses (summonable etc.). Some
  bosses, like Teremus, Abyssal Lords in Silithus, all Racial Leaders,
  retain their original damage. Tested in game practice: Horde Racial
  Leaders (with HP reduced to about 200000), while hard, can be defeated
  by properly built multiclass character in proper pre-raid gear with proper
  enchants and buffs. Note, that most high-level fights are near impossible
  without certain type of elemental Resistance stacked by means of gear and
  buffs.
* Rage and Energy regeneration for all player classes.
* Enabled Spell Crit. chance for Hunters (same values as for Paladins).
* Player is now able to gain Honor by defeating enemies tagged with PvP icon.
  This allows to progress up to Knight/Stone Guard rank relatively easily.
  After that point every enemy of equal or higher level yields just +1 Honor
  point, but Alliance/Horde leaders continue to yield about 400 each, so
  fighting them becomes the most realistic way to reach high PvP ranks and
  access epic quality PvP items.
* Made Battlegrounds accessible in single player mode. It is possible
  to grind Stormpike Reputation to Exalted defeating enemies in Alterac Valley.
  Reputation with three BG factions and Darkmoon Faire can also be gained from
  Runecloth donations, traditionally linked to regular Alliance/Horde factions.
  As for the latter, there are more than enough reputation from quests (not
  diminished at higher player levels).
* 2x Reputation gained by defeating specific creatures. All for the sake of
  making the game less grindy and repetitive, though definitely not shorter and
  less time consuming.
* Each class can use all weapon skills.
* Simultaneous access to all professions and specializations.
* Removed 1- and 2- day cooldowns from Transmutations and Salt Shaker.
* Increased item stacks up to 250, thus toning down "inventory management"
  aspect of game. Multiclass player, which allowed to practice all
  professions, should be able to carry a lots of things.
* Increased armor class for all pieces of Cloth, Leather, Mail >= L40 armor
  to be almost equal to Mail < L40 or Plate >= 50 armor. Even after this
  boost Cloth has less armor than Leather, Leather < Mail etc. Without
  the boost Cloth and Leather would become irrelevant in all-class Single
  Player game due to much lower armor value.
* Quivers and Ammo Pouches can contain any items. Implemented via
  changing subclass of items in patch-112.php and supporting this alternative
  subclass in modified cMangos core, so that only one quiver can be equipped
  and no quivers in banks allowed.
* In case of restoring saved game with active instanced "dungeons" much
  later, when respawn times of all creatures have been expired, the server
  adjusts the respawn times to avoid resetting instance and restores each
  instance in its saved state.
* Configurable price for Guild Tabard (re)design. Now it can be set to free of
  cost, which in conjunction with model replacements in `wows-transmogrify.cmd`
  gives true freedom of designing player's outfit.
* Configurable cost multiplier for Unlearn Talents. Reduced greatly. Multiclass
  player needs to unlearn talents separately for each class.
* Configurable "pet benefits from owner's stats" option: disable (Classic) or
  enable (TBC). More powerful TBC pets seem to be more suitable for
  powerful multiclass player.
* Support for stacks of lootable items (clams, trunks etc.). Looting a stack
  now properly removes only one container item, not the whole stack.
* Configurable option to treat Soul Bags as all-purpose bags (of greater
  capacity, up to 28 slots in case of Core Felcloth Bag) but restricted to
  bank-only use. Now Soul Bags do not lose their value because of allowed
  stacking of Soul Shards.
* Configurable option to turn the Auction House into Warehouse: sell items
  immediately for halve the vendor price at maximum; buyout them for full
  vendor buy price (without reputation discounts) at maximum, 1 silver at
  minimum, it depends on initial bid. Warehouse serves two purposes:
  1) low-cost storage for items, so there is no need to create numerous
  player characters and switching between them, 2) a way to sell items for
  reduced but still substantial price keeping possibility to buyout the sold
  item later, though at much higher price.
* Configurable options to disable stacking of +Parry and/or +Dodge percents
  provided by talents related to different classes. Player gets too much
  avoidance stacking all of them in case of full 8x multiclass player.
* Configurable option to disable/remove soulbinding. Unrestricted BoP and
  Quest items, which can be mailed and placed on AH/Warehouse, are just
  convenient in single player game. Another option restricts taking such
  items from AH/Warehouse.
* Configurable expiration time of conjured items. Default value is 15 minutes,
  which may be too short in practice.
* Removed multiple players requirement for certain rituals in Uldaman and UBRS.
* Instances were fixed in a way so they now much better restore their state
  after reloading server. Opened doors, events etc.
* Some GameMaster commands got extended capabilities:
```text
    .pinfo - show player's secondary stats from the server
    .go distZ  or  .go distXY distZ - teleport up/down, forward/backward
    .go c 0 - teleport to targeted creature
    .fly on/off - was unimplemented in Classic 1.12 server cores for a long time
    .npc add -N  or  .npc add =N - spawn NPC without adding to DB
```

All listed Multiclass-specific settings could be disabled by replacing
mangosd.conf with alternative "original" version, commenting out Tweak...()
calls in patch-112.php file (full remove of this file is not recommended as it
contains some very important DB fixes, including pooling of mineral/herb
resource nodes), removing files in WoW-112\Data\Interface\FrameXML\ folder,
reverting patches in .dbc files and removing WoW-112\Data\patch-i.MPQ, turning off
multiclass Lua addon. After that the repack shall work in traditional
single class multiplayer mode.

On the other hand, by lowering talent rates to x3-x5 it can be tuned into more
difficult "spellcaster multiclass" game (Mage, Warlock, Priest +maybe Druid,
Shaman).

Source code of all C++, SQL, SQL-from-PHP and DBC patches for cMangos repack
is included in .7z archives mentioned above. A knowledgeable person should be
able re-tune every aspect of game.

The most suitable main class for x8 Multiclass game seems to be Hunter.
Programming multipurpose smart actions in form of custom Lua addon helps very
much to manage great number of Spells and Abilities that Multiclass player
learns. WowFileStructures.7z archive contains such addon + advanced keybindings
in WTF/DefaultBindings.wtf (applies to all accounts and characters) along with
other useful Lua addons, fixed and tweaked.


# About Sunwell Core WoW 3.3.5a Multiclass Single Player repack

Sunwell multiclass repack is very similar to cMangos multiclass repack. Started
at April 2023. But cMangos is thoroughly tested/tuned/played from Alliance side
up to L60 with all Dungeons/Molten Core/BWL/ZG working, AQ20 mostly working,
AQ40/Naxxramas partly working (some bosses may not be doable for single player).
Sunwell multiclass repack is at its early stage. Though its C++ core is higher
quality than cMangos, there is lack of tuning for multiclass single player, and
said tuning can only be made well while playing through content...

Requires "enUS" WoW 3.3.5.12340 client (modified SkillRaceClassInfo.dbc and
Spell.dbc are packed into WoW-335a\Data\enUS\patch-enUS-9.MPQ).

